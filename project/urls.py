from django.urls import path
from project.views import index
from django.views.generic import TemplateView
from project.views.index import upload_file


urlpatterns = [
    path('', TemplateView.as_view(template_name='vue_index.html'), name='home'),
    path('process/upload', upload_file, name='upload_file'),
]
